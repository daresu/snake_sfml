#include <SFML/Graphics.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define WIN_X 640                    // ウインドウ幅
#define WIN_Y 480                    // ウインドウ高さ
#define FPS 5                        // フレームレート
#define BLOCK_SIZE 16                // ブロックのサイズ
#define LINE_W (WIN_X / BLOCK_SIZE)  // 縦線の本数
#define LINE_H (WIN_Y / BLOCK_SIZE)  // 横線の本数
#define LINE_MAX LINE_W + LINE_H     // グリッド用の線の数
#define FOOD_MAX 99                  // エサの最大数

// ヘビの構造体
typedef struct _snake_body {
  int x;                     // グリッド上のx座標
  int y;                     // グリッド上のy座標
  sfRectangleShape *body;    // ヘビの画像
  struct _snake_body *next;  // 次のアドレス
} snake_body;

// エサの構造体
typedef struct _food {
  int x;               // グリッド上のx座標
  int y;               // グリッド上のy座標
  sfCircleShape *img;  // エサの画像
} FOOD;

/**
 * @brief ヘビの頭を追加
 *
 * @param snake 現在のヘビの頭
 * @param x 新しいx座標
 * @param y 新しいy座標
 * @return snake_body* 新しいヘビの頭
 */
snake_body *snake_add(snake_body *snake, int x, int y) {
  // 新しいヘビ
  snake_body *snake_new = (snake_body *)malloc(sizeof(snake_body));

  // 新しいヘビを設定
  snake_new->x = x;
  snake_new->y = y;
  snake_new->body = sfRectangleShape_create();
  sfRectangleShape_setSize(snake_new->body,
                           (sfVector2f){BLOCK_SIZE, BLOCK_SIZE});
  sfRectangleShape_setFillColor(snake_new->body, sfColor_fromRGB(0, 255, 255));
  sfRectangleShape_setPosition(
      snake_new->body,
      (sfVector2f){snake_new->x * BLOCK_SIZE, snake_new->y * BLOCK_SIZE});
  snake_new->next = NULL;

  if (snake != NULL) {
    // ヘビを繋げる
    snake_new->next = snake;
  }

  return snake_new;
}

/**
 * @brief ヘビのお尻を削除する
 *
 * @param snake 現在のヘビ
 */
void snake_remove(snake_body *snake) {
  snake_body *snake_now = NULL;
  snake_body *snake_next = NULL;

  if (snake == NULL) {
    return;
  }

  snake_now = snake;
  snake_next = snake;

  while (snake_next->next != NULL) {
    snake_now = snake_next;
    snake_next = snake_next->next;
  }

  snake_now->next = NULL;
  sfRectangleShape_destroy(snake_next->body);
  free(snake_next);
  snake_next = NULL;
}

/**
 * @brief ヘビを全て削除する
 *
 * @param snake 現在のヘビ
 */
void snake_clear(snake_body *snake) {
  snake_body *tmp;

  while (snake != NULL) {
    tmp = snake->next;
    sfRectangleShape_destroy(snake->body);
    free(snake);
    snake = tmp;
  }
}

/**
 * @brief ヘビの当たり判定
 *
 * @param snake ヘビ
 * @param x チェックしたいx座標
 * @param y チェックしたいy座標
 * @return int あたったかどうか
 */
int snake_hit(snake_body *snake, int x, int y) {
  snake_body *tmp = snake;

  while (tmp != NULL) {
    if (tmp->x == x && tmp->y == y) {
      return 1;
    }
    tmp = tmp->next;
  }

  return -1;
}

/**
 * @brief 指定範囲のランダムな数字を返す
 *
 * @param min 最小範囲
 * @param max 最大範囲
 * @return int 範囲内のランダム数
 */
int get_random(int min, int max) {
  return min + (int)(rand() * (max - min + 1.0) / (1.0 + RAND_MAX));
}

/**
 * @brief 指定座標にエサがあるかを判定
 *
 * @param food エサ
 * @param x チェックしたいx座標
 * @param y チェックしたいy座標
 * @return int ヒットしたエサの素数
 */
int food_hit(FOOD *food, int x, int y) {
  for (int i = 0; i < FOOD_MAX; i++) {
    if (food[i].x == x && food[i].y == y) {
      return i;
    }
  }

  return -1;
}

/**
 * @brief エサを作成
 *
 * @param food エサ
 * @param i エサの素数
 */
void food_create(FOOD *food, int i, snake_body *snake) {
  int x = get_random(0, LINE_W - 1);
  int y = get_random(1, LINE_H - 1);

  // エサとヘビの位置がダブらないように調整
  while (food_hit(food, x, y) >= 0 || snake_hit(snake, x, y) >= 0) {
    x = get_random(0, LINE_W - 1);
    y = get_random(1, LINE_H - 1);
  }

  food[i].x = x;
  food[i].y = y;

  sfCircleShape_setPosition(
      food[i].img,
      (sfVector2f){food[i].x * BLOCK_SIZE + 1, food[i].y * BLOCK_SIZE + 1});
}

/**
 * @brief メイン関数
 *
 * @return int プログラム終了時のステータス
 */
int main() {
  // ランダムのseedを変更
  srand((unsigned int)time(NULL));

  // window
  sfRenderWindow *window;
  sfVideoMode mode = {WIN_X, WIN_Y, 32};

  // フォント
  sfFont *font = NULL;
  sfText *txtTitle = NULL;
  sfText *txtScore = NULL;
  sfText *txtLastScore = NULL;

  // エサ
  FOOD food[FOOD_MAX];

  // ヘビ
  snake_body *snake = NULL;
  sfVector2i snake_move = {0};

  // 線
  sfRectangleShape *line[LINE_MAX];

  // イベント
  sfEvent event;

  // スコア
  int iScore = 0;          // 内部用
  char tScore[14] = "\0";  // 表示用

  int GAME_STATE = 0;  // ゲームの状態

  // windowの初期化
  window = sfRenderWindow_create(mode, "snakeゲーム", sfClose, NULL);
  sfRenderWindow_setFramerateLimit(window, FPS);

  // フォントの読み込み
  font = sfFont_createFromFile("data/Hannari.otf");
  txtTitle = sfText_create();
  sfText_setFont(txtTitle, font);
  sfText_setCharacterSize(txtTitle, 24);
  sfText_setFillColor(txtTitle, sfWhite);

  txtScore = sfText_create();
  sfText_setFont(txtScore, font);
  sfText_setCharacterSize(txtScore, 16);
  sfText_setFillColor(txtScore, sfWhite);

  txtLastScore = sfText_create();
  sfText_setFont(txtLastScore, font);
  sfText_setCharacterSize(txtLastScore, 24);
  sfText_setFillColor(txtLastScore, sfWhite);

  // エサの初期化
  for (int i = 0; i < FOOD_MAX; i++) {
    food[i].img = sfCircleShape_create();
    sfCircleShape_setRadius(food[i].img, BLOCK_SIZE / 2 - 1);
    sfCircleShape_setFillColor(food[i].img, sfGreen);
  }

  // 線の初期化と設定
  for (int i = 0; i < LINE_MAX; i++) {
    line[i] = sfRectangleShape_create();
    sfRectangleShape_setFillColor(line[i], sfColor_fromRGB(64, 64, 64));
    if (i < LINE_H) {
      // 横線
      sfRectangleShape_setSize(line[i], (sfVector2f){WIN_X, 1});
      sfRectangleShape_setPosition(line[i],
                                   (sfVector2f){0, BLOCK_SIZE * (i + 1)});
    } else {
      // 縦線
      sfRectangleShape_setSize(line[i], (sfVector2f){WIN_Y, 1});
      sfRectangleShape_setRotation(line[i], 90);
      sfRectangleShape_setPosition(
          line[i], (sfVector2f){BLOCK_SIZE * (i - LINE_H), BLOCK_SIZE});
    }
  }

  // メインループ
  while (sfRenderWindow_isOpen(window)) {
    while (sfRenderWindow_pollEvent(window, &event)) {
      // ウインドウを閉じるイベント
      if (event.type == sfEvtClosed) sfRenderWindow_close(window);

      // キーボードが放されたとき時のイベント
      if (event.type == sfEvtKeyReleased) {
        switch (event.key.code) {
          case sfKeyLeft:
            if (snake_move.x == 0) {
              snake_move.x = -1;
              snake_move.y = 0;
            }

            break;

          case sfKeyRight:
            if (snake_move.x == 0) {
              snake_move.x = 1;
              snake_move.y = 0;
            }
            break;

          case sfKeyUp:
            if (snake_move.y == 0) {
              snake_move.x = 0;
              snake_move.y = -1;
            }
            break;

          case sfKeyDown:
            if (snake_move.y == 0) {
              snake_move.x = 0;
              snake_move.y = 1;
            }
            break;

          default:
            break;
        }
      }
    }

    switch (GAME_STATE) {
      case 0:
        // 初期処理

        // ヘビの初期化
        snake = snake_add(snake, LINE_W / 2, LINE_H / 2);
        snake_move = (sfVector2i){0, 0};

        // エサの初期化
        for (int i = 0; i < FOOD_MAX; i++) {
          food_create(food, i, snake);
        }

        // タイトル
        sfText_setString(txtTitle, "GAME START!");
        sfText_setPosition(txtTitle,
                           (sfVector2f){WIN_X / 2 - (24 * 3), WIN_Y / 2 - 24});

        // スコア
        iScore = 0;
        sfText_setPosition(txtScore, (sfVector2f){0, 0});

        // 最後のスコア
        sfText_setPosition(txtLastScore, (sfVector2f){WIN_X / 2 - (24 * 3.5),
                                                      WIN_Y / 2 + (24 * 1)});

        GAME_STATE = 99;
        break;

      case 99:
        // スタート画面
        if (snake_move.x != 0 || snake_move.y != 0) {
          GAME_STATE = 100;
        }

        // 描画
        sfRenderWindow_clear(window, sfBlack);  // 画面クリア

        sfRenderWindow_drawText(window, txtTitle, NULL);  // 文字の描画

        // 画面フリップ
        sfRenderWindow_display(window);
        break;

      case 100:
        // ゲームメイン
        // 移動処理
        if (snake_hit(snake, snake->x + snake_move.x, snake->y + snake_move.y) >
            0) {
          // 自分の体に当たった
          sfText_setString(txtTitle, "GAME OVER!");
          GAME_STATE = 200;
        }
        if (snake->x + snake_move.x < 0 || LINE_W <= snake->x + snake_move.x ||
            snake->y + snake_move.y < 1 || LINE_H <= snake->y + snake_move.y) {
          // 画面外にはみ出した
          sfText_setString(txtTitle, "GAME OVER!");
          GAME_STATE = 200;
        }
        snake =
            snake_add(snake, snake->x + snake_move.x, snake->y + snake_move.y);
        int food_i =
            food_hit(food, snake->x + snake_move.x, snake->y + snake_move.y);
        if (food_i >= 0) {
          // エサを食べた
          iScore += 10; // 加点
          food_create(food, food_i, snake); // 食べたエサを別の場所に再作成
        } else {
          snake_remove(snake);
        }

        // 描画
        sfRenderWindow_clear(window, sfBlack);  // 画面クリア

        // エサの描画
        for (int i = 0; i < FOOD_MAX; i++) {
          sfRenderWindow_drawCircleShape(window, food[i].img,
                                         NULL);  // 円の描画
        }

        // ヘビの描画
        snake_body *snake_draw = snake;
        while (snake_draw != NULL) {
          sfRenderWindow_drawRectangleShape(window, snake_draw->body, NULL);
          snake_draw = snake_draw->next;
        }

        // グリッドの描画
        for (int i = 0; i < LINE_MAX; i++) {
          sfRenderWindow_drawRectangleShape(window, line[i], NULL);
        }

        // スコアを表示
        if (iScore <= 999999) {
          sprintf(tScore, "SCORE: %06d", iScore);
        } else {
          sprintf(tScore, "SCORE: 999999");
        }
        sfText_setString(txtScore, tScore);
        sfRenderWindow_drawText(window, txtScore, NULL);

        // 画面フリップ
        sfRenderWindow_display(window);
        break;

      case 200:
        // ゲームオーバー画面
        // 描画
        sfRenderWindow_clear(window, sfBlack);  // 画面クリア

        // ゲームオーバーの描画
        sfRenderWindow_drawText(window, txtTitle, NULL);

        // 最後スコアを表示
        if (iScore <= 999999) {
          sprintf(tScore, "SCORE: %06d", iScore);
        } else {
          sprintf(tScore, "SCORE: 999999");
        }
        sfText_setString(txtLastScore, tScore);
        sfRenderWindow_drawText(window, txtLastScore, NULL);

        // 画面フリップ
        sfRenderWindow_display(window);
        break;

      default:
        break;
    }
  }

  // 開放処理
  for (int i = 0; i < LINE_MAX; i++) {
    sfRectangleShape_destroy(line[i]);
  }
  snake_clear(snake);
  for (int i = 0; i < FOOD_MAX; i++) {
    sfCircleShape_destroy(food[i].img);
  }
  sfText_destroy(txtScore);
  sfText_destroy(txtTitle);
  sfFont_destroy(font);
  sfRenderWindow_destroy(window);

  return 0;
}
