# スネークゲーム sfml実装版

sfmlの勉強を兼ねてテスト開発。

## 開発環境

### OS

- Ubuntu 20.04

### 開発ツール

#### Ubuntu

- Visual Studio Code

#### コンパイラ周り

- gcc (or clang)
- cmake

#### 依存ライブラリ

- cfsml
